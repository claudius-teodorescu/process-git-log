# process-git-log



## Scope

This is a module for extracting various information from Git logs, written in Rust.

## Resources
[Analysing source control history with Rust](https://medium.com/thg-tech-blog/analysing-source-control-history-with-rust-ba766cf1f648)
