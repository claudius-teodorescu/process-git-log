use json_value_merge::Merge;
use jsonpath_rust::{JsonPathFinder, JsonPathInst, JsonPathQuery, JsonPathValue};
use lazy_static::lazy_static;
use lexical_sort::{natural_lexical_cmp, StringSort};
use regex::Regex;
use serde_json::{json, Value};
use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};
use std::{env, fs, path};
use std::cmp::Ordering;

lazy_static! {
    static ref EMAIL_REGEX: Regex = Regex::new(
        r"([a-z0-9_+]([a-z0-9_+.-]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6})"
    )
    .unwrap();
    static ref FILENAME_REGEX: Regex = Regex::new(r"data[0-9a-z\/]+A[0-9]+\.xml").unwrap();
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct PersonFullName {
    first_name: String,
    last_name: String,
}

impl PersonFullName {
    pub fn new(first_name: String, last_name: String) -> Self {
        PersonFullName {
            first_name,
            last_name,
        }
    }

    pub fn to_string(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let repo_path = path::Path::new(&args[1]);
    let log_file_path = repo_path.join("commits-log.txt");

    dbg!(&args);

    let users_metadata_file = fs::File::open(
        "/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/sites/site/src/data/redactor-metadata/redactor-metadata.json",
    )
    .expect("file should open read only");
    let users_metadata: serde_json::Value =
        serde_json::from_reader(users_metadata_file).expect("file should be proper JSON");

    process_log_file(log_file_path.as_path(), repo_path, users_metadata);
}

fn process_log_file(
    log_file_path: &path::Path,
    repo_path: &path::Path,
    users_metadata: serde_json::Value,
) {
    if let Ok(lines) = read_index_records(log_file_path) {
        let mut commit_summaries: HashMap<String, HashSet<String>> = HashMap::new();
        let mut current_commiter_email: String = "".to_string();
        let mut current_relative_file_path: String = "".to_string();

        for line in lines.iter() {
            if line == "===" {
                // store the current commit summary
                if current_commiter_email != "".to_string()
                    && current_relative_file_path != "".to_string()
                {
                    commit_summaries
                        .entry(current_commiter_email.to_owned())
                        .and_modify(|entry| {
                            entry.insert(current_relative_file_path.clone());
                        })
                        .or_insert({
                            let mut relative_file_path_set: HashSet<String> = HashSet::new();
                            relative_file_path_set.insert(current_relative_file_path.clone());

                            relative_file_path_set
                        });
                }

                // reset the current variables
                current_commiter_email = "".to_string();
                current_relative_file_path = "".to_string();

                continue;
            }

            let email_match_struct = EMAIL_REGEX.find(line);
            if let Some(email_match) = email_match_struct {
                current_commiter_email = email_match.as_str().to_string();

                continue;
            }

            let filename_match_struct = FILENAME_REGEX.find(line);
            if let Some(filename_match) = filename_match_struct {
                if current_commiter_email != "".to_string()
                    && current_commiter_email != "claudius.teodorescu@gmail.com".to_string()
                    && current_commiter_email != "solirom22@gmail.com".to_string()
                {
                    current_relative_file_path = filename_match.as_str().to_string();
                }

                continue;
            }
        }

        //println!("{:?}", commit_summaries);
        fs::write(
            repo_path.join("users.json"),
            serde_json::to_string(&commit_summaries).expect("Cannot serialize"),
        )
        .expect("Write file.");

        let mut full_names: Vec<PersonFullName> = Vec::new();
        let emails = commit_summaries.keys().collect::<Vec<&String>>();
        for email in emails {
            println!("{}", email);
            let first_name_result = users_metadata
                .clone()
                .path(format!("$[?(@.email == '{}')]['first-name']", email).as_str())
                .unwrap();
            let first_name = first_name_result[0].as_str().unwrap().to_string();
            let last_name_result = users_metadata
                .clone()
                .path(format!("$[?(@.email == '{}')]['last-name']", email).as_str())
                .unwrap();
            let last_name = last_name_result[0].as_str().unwrap().to_string();

            let full_name = PersonFullName::new(first_name, last_name);
            full_names.push(full_name);
        }
        full_names.sort_unstable_by(|a, b| {
            match natural_lexical_cmp(a.last_name.as_str(), b.last_name.as_str()) {
                Ordering::Equal => { natural_lexical_cmp(a.first_name.as_str(), b.first_name.as_str()) }
                v => { v }
            }
        });         

        let full_names_serialized: Vec<String> = full_names
            .into_iter()
            .map(|item| item.to_string())
            .collect();

        fs::write(
            repo_path.join("redactors.html"),
            format!(
                "<article>{}</article>",
                &full_names_serialized.join("<br />")
            ),
        )
        .expect("Write file.");
    }
}

fn read_index_records<P>(filename: P) -> io::Result<Vec<String>>
where
    P: AsRef<path::Path>,
{
    let file = fs::File::open(filename)?;
    let lines: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|line| line.unwrap())
        .collect();

    Ok(lines)
}

#[test]
pub fn process_git_log() {
    let repo_path = path::Path::new(
        "/home/claudius/workspace/projects/CLRE/redactors/list/",
    );
    let log_file_path = repo_path.join("commits-log.txt");

    let users_metadata_file = fs::File::open(
        "/home/claudius/workspace/repositories/git/gitlab.com/solirom-clre/sites/site/src/data/redactor-metadata/redactor-metadata.json",
    )
    .expect("file should open read only");
    let users_metadata: serde_json::Value =
        serde_json::from_reader(users_metadata_file).expect("file should be proper JSON");

    process_log_file(log_file_path.as_path(), repo_path, users_metadata);
}

#[test]
pub fn match_email() {
    let token = &"2022-03-10T11:50:36, maria-alexandra.tudosa@yahoo.com".to_string();

    println!("{:?}", EMAIL_REGEX.find(token).unwrap().as_str());
}

#[test]
pub fn match_filename() {
    let token = &"data/07/transcriptions/entries/A141243.xml | 19 +------------------".to_string();

    println!("{:?}", FILENAME_REGEX.find(token).unwrap().as_str());
}

#[test]
pub fn jsonpath_finder() {
    let json: Value = serde_json::from_str(
        r#"[{"first-name": "Diana-Ionela", "last-name": "Alexandrescu", "email": "diana.alx15@yahoo.com", "role": "transcriber", "affiliation": "UAIC"},
        {"first-name": "Ika Zsuzsanna", "last-name": "Szabó", "email": "ika.bajna@student.unitbv.ro", "role": "transcriber", "affiliation": "UNITBV"},
        {"first-name": "Botond Răzvan", "last-name": "Bedő", "email": "botond.bedo@student.unitbv.ro", "role": "transcriber", "affiliation": "UNITBV"},
        {"first-name": "Mihai", "last-name": "Băjenaru", "email": "mihai.bajenaru@student.unitbv.ro", "role": "transcriber", "affiliation": "UNITBV"},
        {"first-name": "Anett Hajnal", "last-name": "Bencze", "email": "anett.bencze@student.unitbv.ro", "role": "transcriber", "affiliation": "UNITBV"},
        {"first-name": "Adrien", "last-name": "Bota", "email": "adrien.bota@student.unitbv.ro", "role": "transcriber", "affiliation": "UNITBV"}]"#,
    ).unwrap();
    let email = "ika.bajna@student.unitbv.ro";
    let last_name_result = json
        .clone()
        .path(format!("$[?(@.email == '{}')]['last-name']", email).as_str())
        .unwrap();
    let last_name = &last_name_result[0].as_str().unwrap().to_string();
    let first_name_result = json
        .clone()
        .path(format!("$[?(@.email == '{}')]['first-name']", email).as_str())
        .unwrap();
    let first_name = &first_name_result[0].as_str().unwrap().to_string();

    println!("first-name = {:?}", first_name);
    println!("last-name = {:?}", last_name);
}

#[test]
pub fn sort_full_names() {
    let mut full_names: Vec<PersonFullName> = Vec::new();

    let full_name_1 = PersonFullName::new("Mădălina".to_string(), "Popescu".to_string());
    full_names.push(full_name_1);

    let full_name_2 = PersonFullName::new("Andreea".to_string(), "Popescu".to_string());
    full_names.push(full_name_2);

    let full_name_3 = PersonFullName::new("Madi".to_string(), "Popescu".to_string());
    full_names.push(full_name_3);

    let full_name_4 = PersonFullName::new("Madi".to_string(), "Ionescu".to_string());
    full_names.push(full_name_4);    

    full_names.sort_unstable_by(|a, b| {
        match natural_lexical_cmp(a.last_name.as_str(), b.last_name.as_str()) {
            Ordering::Equal => { natural_lexical_cmp(a.first_name.as_str(), b.first_name.as_str()) }
            v => { v }
        }
    });    

    println!("{:?}", full_names);
}

// git --no-pager log --stat --format="===%n%s%-b" | sed '/^$/d' > ./commits-log.txt
